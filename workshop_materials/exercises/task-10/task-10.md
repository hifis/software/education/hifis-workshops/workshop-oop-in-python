---
title: "Task 10"
---

## Task 10: More fun with DNA

### Task 10a: More Details

DNA is made up from two strands: the _template strand_ and the _coding strand_.
What we have currently stored in our sample is the _template strand_.
The _coding strand_ is the element-wise inverse of the _template strand_, where

* _A_ is inverse to _T_
* _C_ is inverse to _G_

so for a _template strand_ `AACGGT` the _coding strand_ would be `TTGCCA`.

Add the methods `template_strand(self)` and `coding_strand(self)` to the `Sample`-class.
They each return the respective sequence as a string.
Do not introduce a new attribute. 
**Bonus:** Discuss benefits and drawbacks for introducing a new attribute to store the _coding strand_.

### Task 10b: Cleanup

By now we have strewn a lot of implicit information about how DNA works across all parts of our model.
Make suggestions how to clean up our code.

* Which benefits would you expect from your suggestion?
* Which model elements would have to be changed and how?
