from random import choices

class DnaSequencer:

    def __init__(self, serial_number, is_clean = False):
        self.serial_number = serial_number
        self.is_clean = is_clean

    def clean(self):
        self.is_clean = True
        print("DNA sequencer", self.serial_number, "has been cleaned")

    def analyze_sample(self, sample):
        if not self.is_clean:
            print("Sequencer", self.serial_number, "is still dirty - abort")
            return

        sequence_length = 2000
        bases = ["A", "C", "G", "T"]
        dna_sequence = "".join(choices(bases, k=sequence_length))
        sample.take_analysis_result(dna_sequence)

        # Analysis done, device got dirty
        self.is_clean = False
