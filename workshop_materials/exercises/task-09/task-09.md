---
title: "Task 09"
---

## Task 9: Who did what?

We want to track the serial number of the last sequencer that worked on a given sample in case one turns out to be broken and produce false results.

Discuss beforehand:

* In which class should this information be stored?
* Which elements must be changed in which way to transfer the information?

Implement your solution.
